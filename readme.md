## Acerca de 
Un ejercicio de programacion web usando TomEE, JPA, JSF, PrimeFaces

## Guía de instalación

### Requerimientos técnicos

### Configuración roles del Apache Tomcat o TomEE 

En el archivo *$CATALINA/conf/tomcat-users.xml* agregar los roles

```
    <role rolename="cliente" />
    <role rolename="manolo-admin" />
```

### Configuración Datasource del Apache Tomcat o TomEE 

En el archivo *$CATALINA/conf/tomee.xml* agregar el recurso según los parámetros de su base de datos

```
    <Resource id="VideoManoloDS" type="javax.sql.DataSource">
		jdbcDriver = com.mysql.jdbc.Driver
		jdbcUrl = jdbc:mysql://localhost:3306/video_manolo
		jtaManaged = true
		password = ***
		passwordCipher = PlainText
		userName = root
	</Resource>
```

### Configuración Realm de Apache Tomcat o TomEE

En el archivo *$CATALINA/conf/server.xml* de la carpeta de Tomcat se coloca anidado en un elemento *<Engine>*
  *<Host>* o *<Context>*. [Ver más aquí](http://tomcat.apache.org/tomcat-7.0-doc/realm-howto.html) 

```
    <Realm className="org.apache.catalina.realm.DataSourceRealm"
       dataSourceName="VideoManoloDS"
       userTable="usuario" userNameCol="usuario" userCredCol="contrasenia"
       userRoleTable="rol" roleNameCol="nombre"/>
```
