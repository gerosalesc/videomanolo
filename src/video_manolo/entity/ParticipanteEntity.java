package video_manolo.entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by chamanx on 24/08/14.
 */
@Entity
@Table(name = "participante", schema = "", catalog = "video_manolo")
public class ParticipanteEntity {
    private int id;
    private String nombre;
    private RolParticipanteEntity rol;
    private Collection<PeliculaEntity> peliculas;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = false, insertable = true, updatable = true, length = 45)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParticipanteEntity that = (ParticipanteEntity) o;

        if (id != that.id) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "rol", referencedColumnName = "id", nullable = false)
    public RolParticipanteEntity getRol() {
        return rol;
    }

    public void setRol(RolParticipanteEntity rol) {
        this.rol = rol;
    }

    @ManyToMany
    @JoinTable(name = "participantes_pelicula", catalog = "video_manolo", schema = "", joinColumns = @JoinColumn(name = "participante_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "pelicula_id", referencedColumnName = "id", nullable = false))
    public Collection<PeliculaEntity> getPeliculas() {
        return peliculas;
    }

    public void setPeliculas(Collection<PeliculaEntity> peliculas) {
        this.peliculas = peliculas;
    }
}
