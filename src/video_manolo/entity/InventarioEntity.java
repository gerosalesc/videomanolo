package video_manolo.entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by chamanx on 24/08/14.
 */
@Entity
@Table(name = "inventario", schema = "", catalog = "video_manolo")
public class InventarioEntity {
    private int id;
    private int cantidad;
    private PeliculaEntity pelicula;
    private Collection<ReservaEntity> reservas;
    private SedeEntity sede;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad", nullable = true, insertable = true, updatable = true, length = 45)
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InventarioEntity that = (InventarioEntity) o;

        if (id != that.id) return false;
        if (cantidad != that.cantidad) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + cantidad;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "pelicula", referencedColumnName = "id", nullable = false)
    public PeliculaEntity getPelicula() {
        return pelicula;
    }

    public void setPelicula(PeliculaEntity pelicula) {
        this.pelicula = pelicula;
    }

    @OneToMany(mappedBy = "inventario")
    public Collection<ReservaEntity> getReservas() {
        return reservas;
    }

    public void setReservas(Collection<ReservaEntity> reservas) {
        this.reservas = reservas;
    }

    @ManyToOne
    @JoinColumn(name = "sede", referencedColumnName = "id", nullable = false)
    public SedeEntity getSede() {
        return sede;
    }

    public void setSede(SedeEntity sede) {
        this.sede = sede;
    }
}
