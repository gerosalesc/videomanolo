package video_manolo.entity;

/**
 * Created by chamanx on 25/08/14.
 */
public enum EstadoReserva {
    MULTA, //No se entregó a tiempo
    RECOGIDO, //Se recogido el dia de la reserva
    ENTREGADO, //Se ha entregado en el tiempo estimado
    NUEVO, //Se hizo a reserva pero aun no se ha recogido
}
