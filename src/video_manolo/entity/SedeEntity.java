package video_manolo.entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by chamanx on 24/08/14.
 */
@Entity
@Table(name = "sede", schema = "", catalog = "video_manolo")
public class SedeEntity {
    private int id;
    private String nombre;
    private String ubicacion;
    private Collection<InventarioEntity> inventarios;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = false, insertable = true, updatable = true, length = 45)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "ubicacion", nullable = true, insertable = true, updatable = true, length = 45)
    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SedeEntity that = (SedeEntity) o;

        if (id != that.id) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;
        if (ubicacion != null ? !ubicacion.equals(that.ubicacion) : that.ubicacion != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (ubicacion != null ? ubicacion.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "sede")
    public Collection<InventarioEntity> getInventarios() {
        return inventarios;
    }

    public void setInventarios(Collection<InventarioEntity> inventarios) {
        this.inventarios = inventarios;
    }
}
