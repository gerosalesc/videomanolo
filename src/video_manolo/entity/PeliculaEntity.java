package video_manolo.entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by chamanx on 24/08/14.
 */
@Entity
@Table(name = "pelicula", schema = "", catalog = "video_manolo")
public class PeliculaEntity {
    private int id;
    private String titulo;
    private String descripcion;
    private int anio;
    private String imagenUrl;
    private Collection<InventarioEntity> inventarios;
    private Collection<ParticipanteEntity> participantes;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() { return id; }

    public void setId(int id) {this.id = id;}

    @Basic
    @Column(name = "titulo", nullable = false, insertable = true, updatable = true, length = 45)
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Basic
    @Column(name = "descripcion", nullable = true, insertable = true, updatable = true, length = 65535)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "anio", nullable = true, insertable = true, updatable = true, length = 4)
    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    @Basic
    @Column(name = "imagen_url", nullable = true, insertable = true, updatable = true, length = 65535)
    public String getImagenUrl() {
        return imagenUrl;
    }

    public void setImagenUrl(String imagenUrl) {
        this.imagenUrl = imagenUrl;
    }

    @OneToMany(mappedBy = "pelicula")
    public Collection<InventarioEntity> getInventarios() {
        return inventarios;
    }

    public void setInventarios(Collection<InventarioEntity> inventarios) {
        this.inventarios = inventarios;
    }

    @ManyToMany(mappedBy = "peliculas")
    public Collection<ParticipanteEntity> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(Collection<ParticipanteEntity> participantes) {
        this.participantes = participantes;
    }
}
