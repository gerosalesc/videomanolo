package video_manolo.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by chamanx on 24/08/14.
 */
@Entity
@Table(name = "reserva", schema = "", catalog = "video_manolo")
public class ReservaEntity {
    private int id;
    private EstadoReserva estado;
    private Timestamp fechaReserva;
    private UsuarioEntity usuario;
    private InventarioEntity inventario;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha_reserva", nullable = true, insertable = true, updatable = true)
    public Timestamp getFechaReserva() {
        return fechaReserva;
    }

    public void setFechaReserva(Timestamp fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "estado", nullable = true, insertable = true, updatable = true)
    public EstadoReserva getEstado() {
        return estado;
    }

    public void setEstado(EstadoReserva estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReservaEntity that = (ReservaEntity) o;

        if (id != that.id) return false;
        if (fechaReserva != null ? !fechaReserva.equals(that.fechaReserva) : that.fechaReserva != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (fechaReserva != null ? fechaReserva.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "usuario", referencedColumnName = "id", nullable = false)
    public UsuarioEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioEntity usuario) {
        this.usuario = usuario;
    }

    @ManyToOne
    @JoinColumn(name = "inventario", referencedColumnName = "id", nullable = false)
    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
