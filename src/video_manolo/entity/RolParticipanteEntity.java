package video_manolo.entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by chamanx on 24/08/14.
 */
@Entity
@Table(name = "rol_participante", schema = "", catalog = "video_manolo")
public class RolParticipanteEntity {
    private int id;
    private String codigo;
    private String nombre;
    private Collection<ParticipanteEntity> participantes;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo", nullable = false, insertable = true, updatable = true, length = 45)
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "nombre", nullable = true, insertable = true, updatable = true, length = 45)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RolParticipanteEntity that = (RolParticipanteEntity) o;

        if (id != that.id) return false;
        if (codigo != null ? !codigo.equals(that.codigo) : that.codigo != null) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (codigo != null ? codigo.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "rol")
    public Collection<ParticipanteEntity> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(Collection<ParticipanteEntity> participantes) {
        this.participantes = participantes;
    }
}
