package video_manolo.converter;

import javax.ejb.EJB;
import javax.faces.convert.Converter;
import javax.faces.context.FacesContext;
import javax.faces.component.UIComponent;
import javax.faces.convert.FacesConverter;

import video_manolo.entity.SedeEntity;
import video_manolo.facade.SedeLocal;

@FacesConverter("video_manolo.converter.sede")
public class SedeConverter implements Converter {

    @EJB
    SedeLocal sedeEJB;

    public SedeConverter() {
    }

    public Object getAsObject(FacesContext facesContext, UIComponent uIComponent, String string) {
        if (string == null || string.trim().length() == 0) {
            return null;
        }

        final int id = Integer.parseInt(string);

        return sedeEJB.find(id);
    }

    public String getAsString(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object == null) return null;

        if (object instanceof SedeEntity) {
            SedeEntity entity = (SedeEntity) object;

            final String pk = String.valueOf(entity.getId());

            return pk;
        } else {
            throw new IllegalArgumentException("Incorrect object type: " + object.getClass().getName() + "; must be: SedeEntity");
        }
    }
}
