package video_manolo.facade;

import video_manolo.entity.EstadoReserva;
import video_manolo.entity.InventarioEntity;
import video_manolo.entity.ReservaEntity;
import video_manolo.entity.UsuarioEntity;
import video_manolo.exception.InventarioInsuficienteException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by chamanx on 25/08/14.
 */
@TransactionManagement
@Stateless(name = "ReservaFacadeEJB")
public class ReservaFacadeBean extends AbstractFacade<ReservaEntity> implements ReservaLocal {

    @PersistenceContext(unitName = "VideoManoloPU")
    private EntityManager entityManager;
    @EJB
    private InventarioLocal inventarioEJB;
    @EJB
    private UsuarioLocal usuarioEJB;

    public ReservaFacadeBean() {
        super(ReservaEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Collection<ReservaEntity> getReservasNoEntregadasBySedePelicula(int idSede, int idPelicula) {
        Query q = getEntityManager().createQuery(
                "select re from ReservaEntity re join re.inventario inv " +
                        "where inv.sede.id = ?1 and inv.pelicula.id = ?2 and not re.estado = ?3")
                .setParameter(1, idSede).setParameter(2, idPelicula).setParameter(3, EstadoReserva.ENTREGADO);
        List reservas = q.getResultList();
        if(reservas == null){ //Evitar que retorne nulo
            reservas = new ArrayList();
        }
        return reservas;
    }

    @Override
    public void nuevaReserva(int idSede, int idPelicula, Date date, int idUsuario) throws InventarioInsuficienteException {
        //TODO: validar que no tenga multas
        //Validar que tenga inventario
        Collection<ReservaEntity> reservas = getReservasNoEntregadasBySedePelicula(idSede, idPelicula);
        int count = reservas.size();
        //TODO: buscar cuanto hay en inventario total y comprar contra los items que no se han entregado
        int totalInventario = inventarioEJB.getCantidadInventarioDePeliculaEnSede(idSede, idPelicula);
        UsuarioEntity usuario = usuarioEJB.find(idUsuario);
        InventarioEntity inventario = inventarioEJB.getOneInventarioDePeliculaEnSede(idSede, idPelicula);
        if(count < totalInventario){
            if (usuario != null){
                if (inventario != null) {
                    ReservaEntity re = new ReservaEntity();
                    re.setEstado(EstadoReserva.NUEVO);
                    re.setFechaReserva(new Timestamp(date.getTime()));
                    re.setUsuario(usuario);
                    re.setInventario(inventario);
                    create(re);
                }else{
                    //TODO: retornar otra custom exception
                }
            }else{
                //TODO: retornar otra custom exception
            }
        }else{
            throw new InventarioInsuficienteException();
        }

    }

    @Override
    public Collection<ReservaEntity> getReservasMultadasUsuario(int idUsuario) {
        Query q = getEntityManager().createQuery(
                "SELECT re from ReservaEntity re where re.estado = ?1")
                .setParameter(1, EstadoReserva.MULTA);
        return q.getResultList();
    }


}
