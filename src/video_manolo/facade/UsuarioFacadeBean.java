package video_manolo.facade;

import video_manolo.entity.RolEntity;
import video_manolo.entity.Roles;
import video_manolo.entity.UsuarioEntity;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by chamanx on 27/08/14.
 */
@Stateless(name = "UsuarioFacadeEJB")
public class UsuarioFacadeBean extends AbstractFacade<UsuarioEntity> implements UsuarioLocal{

    @PersistenceContext(unitName = "VideoManoloPU")
    private EntityManager em;
    @EJB
    private RolLocal rolEJB;

    public UsuarioFacadeBean() {
        super(UsuarioEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public UsuarioEntity findByUsuario(String userName) {
        Query q = getEntityManager().createQuery(
                "select usr from UsuarioEntity usr where usr.usuario = ?1")
                .setParameter(1, userName).setMaxResults(1);
        try {
            return (UsuarioEntity) q.getSingleResult();
        }catch (NoResultException nre){//TODO: log
        }
        return null;
    }

    @Override
    public void createUsuario(UsuarioEntity usuario) {
        //Persistir usuario
        create(usuario);
        //Crear registro en tabla de roles
        rolEJB.create(new RolEntity(Roles.cliente.toString(), usuario.getUsuario()));
    }
}
