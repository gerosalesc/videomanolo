package video_manolo.facade;

import video_manolo.entity.SedeEntity;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by chamanx on 25/08/14.
 */
@TransactionManagement
@Stateless(name = "SedeFacadeEJB")
public class SedeFacadeBean extends AbstractFacade<SedeEntity> implements SedeLocal{
    @PersistenceContext(unitName = "VideoManoloPU")
    private EntityManager em;

    public SedeFacadeBean() {
        super(SedeEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
