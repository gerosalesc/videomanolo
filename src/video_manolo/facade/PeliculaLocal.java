package video_manolo.facade;

import video_manolo.entity.PeliculaEntity;

import javax.ejb.Local;

/**
 * Created by chamanx on 25/08/14.
 */
@Local
public interface PeliculaLocal extends CrudEJBLocal<PeliculaEntity> {

}
