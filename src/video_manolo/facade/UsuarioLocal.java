package video_manolo.facade;

import video_manolo.entity.UsuarioEntity;

import javax.ejb.Local;

/**
 * Created by chamanx on 27/08/14.
 */
@Local
public interface UsuarioLocal extends CrudEJBLocal<UsuarioEntity>{
    UsuarioEntity findByUsuario(String userName);

    /**
     * Crear un usuario nuevo del formulario
     * @param usuario
     */
    void createUsuario(UsuarioEntity usuario);
}
