package video_manolo.facade;

import video_manolo.entity.RolEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by chamanx on 24/08/14.
 */
@Stateless(name = "RolFacadeEJB")
public class RolFacadeBean extends AbstractFacade<RolEntity> implements RolLocal{
    @PersistenceContext(unitName = "VideoManoloPU")
    private EntityManager em;

    public RolFacadeBean() {
        super(RolEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
