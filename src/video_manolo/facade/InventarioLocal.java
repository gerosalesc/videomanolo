package video_manolo.facade;

import video_manolo.entity.InventarioEntity;

import javax.ejb.Local;

/**
 * Created by chamanx on 27/08/14.
 */
@Local
public interface InventarioLocal extends CrudEJBLocal<InventarioEntity>{

    /**
     * Obtiene la cantidad de existencias de una pelicula en una sede
     * @param idSede
     * @param idPelicula
     * @return
     */
    int getCantidadInventarioDePeliculaEnSede(int idSede, int idPelicula);

    /**
     * TODO:
     * @param idSede
     * @param idPelicula
     * @return
     */
    InventarioEntity getOneInventarioDePeliculaEnSede(int idSede, int idPelicula);
}
