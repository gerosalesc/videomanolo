package video_manolo.facade;

import video_manolo.entity.InventarioEntity;
import video_manolo.entity.SedeEntity;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by chamanx on 27/08/14.
 */
@TransactionManagement
@Stateless(name = "InventarioFacadeEJB")
public class InventarioFacadeBean extends AbstractFacade<InventarioEntity> implements InventarioLocal{

    @PersistenceContext(unitName = "VideoManoloPU")
    private EntityManager em;

    public InventarioFacadeBean() {
        super(InventarioEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public int getCantidadInventarioDePeliculaEnSede(int idSede, int idPelicula) {
        Query q = getEntityManager().createQuery(
                "select sum(inv.cantidad) from InventarioEntity inv where inv.sede.id=?1 and inv.pelicula.id = ?2")
                .setParameter(1, idSede).setParameter(2, idPelicula);
        return ((Long)q.getSingleResult()).intValue();
    }

    @Override
    public InventarioEntity getOneInventarioDePeliculaEnSede(int idSede, int idPelicula) {
        //TODO: Escojer el registro de inventario que tenga disponible registros
        Query q = getEntityManager().createQuery(
                "select inv from InventarioEntity inv " +
                        "where inv.sede.id=?1 and inv.pelicula.id = ?2 and inv.cantidad > 0")
                .setParameter(1, idSede).setParameter(2, idPelicula).setMaxResults(1);
        return (InventarioEntity) q.getSingleResult();
    }

}
