package video_manolo.facade;

import video_manolo.entity.ReservaEntity;
import video_manolo.exception.InventarioInsuficienteException;

import javax.ejb.Local;
import java.util.Collection;
import java.util.Date;

/**
 * Created by chamanx on 25/08/14.
 */
@Local
public interface ReservaLocal extends CrudEJBLocal<ReservaEntity>{

    /**
     * Busca las reservas de una sede y una pelicula en particular
     * @param idSede
     * @param idPelicula
     * @param date
     * @return
     */
    public Collection<ReservaEntity> getReservasNoEntregadasBySedePelicula(int idSede, int idPelicula);

    /**
     * Nueva reserva
     * @param idSede
     * @param idPelicula
     * @param date
     */
    public void nuevaReserva(int idSede, int idPelicula, Date date, int idUsuario) throws InventarioInsuficienteException;

    /**
     *
     * @param idUsuario
     * @return
     */
    Collection<ReservaEntity> getReservasMultadasUsuario(int idUsuario);
}
