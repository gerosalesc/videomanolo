package video_manolo.facade;

import video_manolo.entity.PeliculaEntity;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by chamanx on 24/08/14.
 */
@Local(PeliculaLocal.class)
@TransactionManagement
@Stateless(name = "PeliculaSessionEJB")
public class PeliculaFacade extends AbstractFacade<PeliculaEntity> implements PeliculaLocal{
    @PersistenceContext(unitName = "VideoManoloPU")
    private EntityManager entityManager;

    public PeliculaFacade() {
        super(PeliculaEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return this.entityManager;
    }
}
