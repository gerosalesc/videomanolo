package video_manolo.facade;

import video_manolo.entity.SedeEntity;

import javax.ejb.Local;

/**
 * Created by chamanx on 25/08/14.
 */
@Local
public interface SedeLocal extends CrudEJBLocal<SedeEntity>{
}
