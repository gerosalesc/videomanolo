package video_manolo.facade;

import video_manolo.entity.RolEntity;

import javax.ejb.Local;

/**
 * Created by chamanx on 28/08/14.
 */
@Local
public interface RolLocal extends CrudEJBLocal<RolEntity>{

}
