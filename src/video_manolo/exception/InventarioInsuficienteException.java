package video_manolo.exception;

import javax.faces.context.FacesContext;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by chamanx on 27/08/14.
 * Excepcion para la validacion de incentario insufuciente al crear reservas
 */
public class InventarioInsuficienteException extends Exception {

    private static final String msg = "Inventario insuficiente, esta pelicula ya ha sido reservada";

    public InventarioInsuficienteException() {
        super(msg);
    }

    public InventarioInsuficienteException(String message){
        super(msg+": "+message);
    }

    public InventarioInsuficienteException(String message, Throwable cause) {
        super(msg+": "+message, cause);
    }

    public InventarioInsuficienteException(Throwable cause) {
        super(cause);
    }

    public InventarioInsuficienteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(msg+": "+message, cause, enableSuppression, writableStackTrace);
    }
}
