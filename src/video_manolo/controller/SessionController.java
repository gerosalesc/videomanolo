package video_manolo.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by chamanx on 25/08/14.
 */
@ManagedBean(name="sessionController")
@SessionScoped
public class SessionController {

    public boolean isLoggedIn(){
        Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletRequest req = (HttpServletRequest) request;
        return (req.getSession(false) != null)? true: false;
    }

    public String salir(){
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/index.xhtml?faces-redirect=true";
    }
}
