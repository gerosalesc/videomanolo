package video_manolo.controller;

import video_manolo.entity.PeliculaEntity;
import video_manolo.entity.SedeEntity;
import video_manolo.entity.UsuarioEntity;
import video_manolo.exception.InventarioInsuficienteException;
import video_manolo.facade.ReservaLocal;
import video_manolo.facade.UsuarioLocal;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by chamanx on 25/08/14.
 */
@ManagedBean(name="reservasController")
@SessionScoped
public class ReservasController implements Serializable{
    @EJB
    private ReservaLocal reservaEJB;
    @EJB
    private UsuarioLocal usuarioEJB;
    private SedeEntity selectedSede;
    private Date selectedDate;
    private PeliculaEntity selectedPeli;

    protected ReservaLocal getFacade(){
        return reservaEJB;
    }

    public SedeEntity getSelectedSede() {
        return selectedSede;
    }

    public void setSelectedSede(SedeEntity selectedSede) {
        this.selectedSede = selectedSede;
    }

    public Date getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }

    public PeliculaEntity getSelectedPeli() {
        return selectedPeli;
    }

    public void setSelectedPeli(PeliculaEntity selectedPeli) {
        this.selectedPeli = selectedPeli;
    }

    /**
     * preparar el formulario de nueva reserva
     * @return
     */
    public String prepararNueva(PeliculaEntity peli){
        setSelectedPeli(peli);
        return "reserva/new?faces-redirect=true";
    }

    /**
     * Nueva reserva
     *
     */
    public String nueva(String userName){
        try {
            UsuarioEntity usuario = usuarioEJB.findByUsuario(userName);
            if (usuario != null) {
                getFacade().nuevaReserva(
                        getSelectedSede().getId(), getSelectedPeli().getId(), getSelectedDate(), usuario.getId());
            }else{
                JsfUtil.addErrorMessage("Error usuario no encontrado");
                return "";
            }
        }catch (InventarioInsuficienteException ie){
            JsfUtil.addErrorMessage(ie.getMessage());
            return "";
        }
        JsfUtil.addSuccessMessage("Su Reserva ha sido realizada con éxito");
        return "index?faces-redirect=true";
    }

}
