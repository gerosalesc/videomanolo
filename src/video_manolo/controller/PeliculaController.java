package video_manolo.controller;

import video_manolo.entity.PeliculaEntity;
import video_manolo.facade.PeliculaLocal;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created by chamanx on 25/08/14.
 */
@ManagedBean(name="peliculaController")
@RequestScoped
public class PeliculaController implements Serializable{
    @EJB
    private PeliculaLocal peliculaEJB;


    protected PeliculaLocal getFacade(){
        return peliculaEJB;
    }

    public Collection<PeliculaEntity> getListAll(){
        return getFacade().findAll();
    }

}
