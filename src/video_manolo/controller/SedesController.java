package video_manolo.controller;

import video_manolo.entity.SedeEntity;
import video_manolo.facade.SedeLocal;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created by chamanx on 25/08/14.
 */
@ManagedBean(name = "sedesController")
@RequestScoped
public class SedesController implements Serializable{
    @EJB
    private SedeLocal ejb;

    protected SedeLocal getFacade(){
        return ejb;
    }

    public Collection<SedeEntity> getListAll(){
        return getFacade().findAll();
    }

}
