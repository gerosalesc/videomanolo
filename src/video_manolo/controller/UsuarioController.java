package video_manolo.controller;

import video_manolo.entity.UsuarioEntity;
import video_manolo.facade.UsuarioLocal;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by chamanx on 28/08/14.
 */
@ManagedBean(name = "usuarioController")
@ViewScoped
public class UsuarioController implements Serializable{
    @EJB
    private UsuarioLocal usuarioEJB;
    private UsuarioEntity nuevoUsuario;

    protected UsuarioLocal getFacade(){
        return usuarioEJB;
    }

    public UsuarioEntity getNuevoUsuario() {
        return nuevoUsuario;
    }

    public void setNuevoUsuario(UsuarioEntity nuevoUsuario) {
        this.nuevoUsuario = nuevoUsuario;
    }

    public String nueva(){
        getFacade().createUsuario(
                nuevoUsuario);
        return "index?faces-redirect=true";
    }
}
